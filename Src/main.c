/* 
 * File:   main.c
 * Author: Alejandro Ramirez
 *
 * Created on 13 de julio de 2021, 09:55 AM
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "plib.h"
#include "main.h"
#include "config_bits.h"



int count=0;
int count_g=0;
int count_y=0;
int heart_b=0;
char buffer[4];
int index=0; 
int period_g;
int period_y;

void main (void) {
    
    gpio_init();
    timer1_init();
    uart1_init();
    while (1){
        button_polling();
        timer1_polling_flag();
        uart1_tx();
        uart1_rx_polling();
        gpio_uart(period_g, period_y);
    }    
}
void gpio_init(void){
    //Yellow LED as output LED3
    TRISBbits.TRISB10 = 0;
    //Green LED as output LED2
    TRISBbits.TRISB3 = 0;
    //red LED as output LED1
    TRISBbits.TRISB2 = 0;
    
    TRISEbits.TRISE4 = 0;
    TRISEbits.TRISE6 = 0;
    TRISEbits.TRISE7 = 0;
    //s1 as input 
    TRISDbits.TRISD6 = 1;
}


void button_polling(void){
    if (PORTDbits.RD6 == 0) {
        LATBbits.LATB10 = 1;
        LATBbits.LATB3 = 0;
        LATBbits.LATB2 = 0;
    }
    else {
        LATBbits.LATB10 = 0;
        LATBbits.LATB3 = 0;
        LATBbits.LATB2 = 0;
    }
}
void timer1_init(void){
    //turn off Timer1 module
    T1CONbits.ON = 0;
    //prescale 1:64
    T1CONbits.TCKPS = 0b10;
    TMR1 = 0;
    PR1 = T1_TICK_RATE;
    T1CONbits.ON = 1;
}
void timer1_polling_flag(void){
    // 1ms
    if (IFS0bits.T1IF == 1) {
        T1CONbits.ON = 0;
        if (heart_b==200){
            LATEbits.LATE4 = ~LATEbits.LATE4;
            heart_b=0;
        }
        TMR1 = 0;
        IFS0bits.T1IF = 0;
        T1CONbits.ON = 1;
        heart_b++;
        count++;
        count_y++;
        count_g++;
    }
}
void uart1_init(void){
    //Mapping 
    //Rx --RPF1
    U1RXR = 0b0100;
    //TX --> RPF0
    RPF0R = 0b0011;
    //Disable rx and tx
    U1STAbits.UTXEN = 0;
    U1STAbits.URXEN = 0;
    U1BRG = BAUD_RATE_REG_VALUE;
    //8, n
    U1MODEbits.PDSEL = 0b00;     
    //stop bit 1 
    U1MODEbits.STSEL = 0;
    //interrupt flags enable when 1 character is received
    U1STAbits.URXISEL = 0b00;
    //enable rx and tx
    U1STAbits.UTXEN = 1;
    U1STAbits.URXEN = 1;
    U1MODEbits.ON = 1;
    //clear flags rx / tx
    IFS1bits.U1RXIF = 0;
    IFS1bits.U1TXIF = 0;
}
void uart1_tx(){
    char buffer[]="PRINT";
    int index=0; 
    if (count==1000){
        
        while (index<5){
            U1TXREG = buffer[index];
            index++;
            count=0;
        }
    }
    //while (index<=4){
    //    U1TXREG = buffer[index];
    //} 
}
void uart1_rx_polling(void){

    if (IFS1bits.U1RXIF == 1) {
        buffer[index] = U1RXREG;
        index++;
        IFS1bits.U1RXIF = 0;
    }
    if (index==4){  
        index=0;
        if (buffer[0]=='*'){
           if (buffer[1]=='2'){
               period_g=(int)buffer[2];
           } else if (buffer[1]=='3'){
               period_y=(int)buffer[2];
           }
        }   
    }
}
void gpio_uart(int period_g,int period_y){
    if (count_g>=period_g*100){
        LATEbits.LATE6 = ~LATEbits.LATE6; 
        count_g=0;
    }
    if (count_y>=period_y*100){
        LATEbits.LATE7 = ~LATEbits.LATE7; 
        count_y=0;
    }
}